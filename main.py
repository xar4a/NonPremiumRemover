from configparser import ConfigParser
from datetime import datetime, timedelta

from pyrogram import Client, filters
from tortoise import Tortoise, run_async

from db.dbConnect import dbconnect
from db.models import Chat
from text import kicktext


config = ConfigParser()
config.read("config.ini")

api_id = 2040
api_hash = "b18441a1ff607e10a989891a5462e627"
bot_token = config["telegram"]["token"]

app = Client(
    "my_bot",
    api_id=api_id, api_hash=api_hash,
    bot_token=bot_token
)


@app.on_message(filters.chat() & filters.new_chat_members & filters.me)
async def welcome():
    print("1")
    # need to fix


async def dbinit():
    await dbconnect()
    await Tortoise.generate_schemas()


async def kicknonpremium():
    async for chatid in Chat.id:
        async for member in app.get_chat_members(chatid):
            if member.user.is_bot is False:
                if member.user.is_premium is False:
                    muser = member.user
                    await app.send_message(chatid, kicktext.format(muser.username))
                    await app.ban_chat_member(chatid, muser.id, datetime.now() + timedelta(seconds=1))


if __name__ == "__main__":
    run_async(dbinit())
    app.run()
