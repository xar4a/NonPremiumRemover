from tortoise import Tortoise


async def dbconnect():
    await Tortoise.init(
        db_url='sqlite://db.sqlite3',
        modules={'models': ['db.models']}
    )
